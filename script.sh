#!/bin/bash
sudo apt-get install -y  containerd && curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add - ;
touch /etc/apt/sources.list.d/kubernetes.list && echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" >> /etc/apt/sources.list.d/kubernetes.list ;
sudo apt-get update ;
sudo apt-get install -y kubelet kubeadm kubectl ; sudo apt-mark hold kubelet kubeadm kubectl containerd ;
sudo swapoff -a ; 
sudo reboot ;

